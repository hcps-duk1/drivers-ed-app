// JavaScript Document
var ss = document.getElementsByClassName('stopwatch');

//[].forEach.call(ss, function (s) {
	var currentTimer = 0;
	var interval = 0;
	var lastUpdateTime = new Date().getTime();
	/*var start = document.getElementById('start');
	var stop = document.getElementById('stop');
	var reset = document.getElementById('reset');*/
	//var hrs = document.getElementById("hours");
	//var mins = document.getElementById("minutes");
	//var secs = document.getElementById("seconds");
	
	function pad(n){
		return ('00' + n).substr(-2);
	}

	function update(){
		var now = new Date().getTime();
		var dt = now - lastUpdateTime;
		currentTimer = dt;
		var time = new Date(currentTimer);
		document.getElementById("hours").innerHTML = pad(time.getHours());
		document.getElementById("minutes").innerHTML = pad(time.getMinutes());
		document.getElementById("seconds").innerHTML = pad(time.getSeconds());
		
		
		lastUpdateTime = now;
	}


	function startTimer(){
		if(!interval){
			lastUpdateTime = new Date().getTime();
			interval = setInterval(update(), 1);
		}
	}

	function stopTimer(){
		clearInterval(interval);
		interval = 0;
	}

	function resetTimer(){
		stopTimer();
		document.getElementById("hours").innerHTML = "00";
		document.getElementById("minutes").innerHTML = "00";
		document.getElementById("seconds").innerHTML = "00";

	}
//});